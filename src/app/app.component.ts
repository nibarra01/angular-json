import { Component } from '@angular/core';
import  StaffData  from "../jason.json";


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'angular-json';
}
