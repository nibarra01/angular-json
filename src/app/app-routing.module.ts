import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { IndexComponent } from './components/index/index.component';
import { MainComponent } from './components/main/main.component';

const routes: Routes = [
  {
    path: 'codebound',
    component: MainComponent
  },
  {
    path:'',
    component:IndexComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
