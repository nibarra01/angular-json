export interface Student {
    id: number;
    studentNum: number;
    name: string;
    email: string;
    grade: string;
  }