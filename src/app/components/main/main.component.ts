import { Component, OnInit } from '@angular/core';
import StaffData  from "../../../jason.json";
import { Staff } from "../../models/STAFF";
import StudentData from "../../../deli.json"
import { Student } from "../../models/STUDENT";

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.css']
})
export class MainComponent implements OnInit {
  staff : Staff[] = StaffData;
  students : Student[] = StudentData;

  constructor() { }

  ngOnInit(): void {
  }

}
